asgiref==3.5.0
Django==4.0
pip==21.3.1
pytz==2021.3
setuptools==60.5.0
sqlparse==0.4.2
typing_extensions==4.0.1
wheel==0.37.1
